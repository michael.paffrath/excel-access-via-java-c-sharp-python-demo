package com.company;

import org.apache.poi.ss.usermodel.*;

import java.io.File;

public class Main {

    public static void main(String[] args) {
	// write your code here
        try {
            File directory = new File(System.getProperty("user.dir"));
            String filename = directory.getParent() + "/demo.xlsx";

            Workbook workbook = WorkbookFactory.create(new File(filename));
            Sheet sheet = workbook.getSheetAt(0);
            Row row = sheet.getRow(1);
            Cell cell = row.getCell(0);

            System.out.println(cell.getStringCellValue());
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
