﻿using Microsoft.Office.Interop.Excel;
using System;
using System.IO;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace prototype_csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"../../../../../../")) + "demo.xlsx";
            var excelApp = new Microsoft.Office.Interop.Excel.Application();

            Workbook workbook = excelApp.Workbooks.Open(filename);
            Worksheet worksheet = workbook.Worksheets[1];
            Range range = worksheet.UsedRange;

            string strResult = (string)(range.Cells[1, 1] as Range).Value2;
            Console.WriteLine(strResult);

            //Garbage Collector things
            workbook.Close();
            excelApp.Quit();
        }
    }
}
